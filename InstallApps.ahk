﻿
ia_ver = 0.99
ia_tempdir = %A_Temp%\ia_spin

#NoTrayIcon
#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
;~ #Warn  ; Recommended for catching common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
global Edit_ID

;DEBUGGING
If not A_IsCompiled
	goto init

If !A_IsAdmin
{
	MsgBox, 16, InstallApps %ia_ver%, InstallApps must be run as Administrator.
	ExitApp
}

IfNotInString, A_ScriptDir, \\aait
{
	MsgBox, 16, InstallApps %ia_ver%, InstallApps must be run from AAIT.
	ExitApp
}

Gui, Login:New
Gui, Login:Add, Text, x2 y10 w90 h20 , Username:
Gui, Login:Add, Text, x2 y30 w90 h20 , Password:
Gui, Login:Add, Text, x2 y50 w90 h20 , Domain:
Gui, Login:Add, Edit, x92 y10 w120 h20 via_user, 
Gui, Login:Add, Edit, x92 y30 w120 h20 +Password via_pass, 
Gui, Login:Add, Edit, x92 y50 w120 h20 via_domain, 
Gui, Login:Add, Button, x112 y70 w100 h30 Default vbtnLoginOK gLoginOK, OK
Gui, Login:Add, Button, x2 y70 w100 h30 gLoginCancel, Cancel
; Generated using SmartGUI Creator for SciTE
Gui Login:-SysMenu +OwnDialogs
Gui, Login:Show, w220 h104, InstallApps %ia_ver%
return

LoginOK:
	GuiControlGet, ia_user
	GuiControlGet, ia_pass
	GuiControlGet, ia_domain
	If !ia_user
	{
		MsgBox, 16,, Username required.
		Guicontrol, Focus, ia_user
		return
	}
	If !ia_pass
	{
		MsgBox, 16,, Password required.
		Guicontrol, Focus, ia_pass
		return
	}
	If !ia_domain
	{
		MsgBox, 16,, Domain required.
		Guicontrol, Focus, ia_domain
		return
	}
	GuiControl, Disable, ia_user
	GuiControl, Disable, ia_pass
	GuiControl, Disable, ia_domain
	GuiControl, Disable, btnLoginOK
	
	;~ msgbox Username: %ia_user%`nPassword: %ia_pass%`nDomain: %ia_domain%
	RunWait, %comspec% /c net use T: \\BSW2KP93.spin.local\mis %ia_pass% /USER:%ia_domain%\%ia_user% /PERSISTENT:NO,,Hide UseErrorLevel
	If ErrorLevel
		{
			Msgbox, 16,, Login failed.
			ExitApp
		} else {
			RunWait, %comspec% /c net use T: /DELETE,,Hide
			Gui, Login:Destroy
			goto Init
		}
return

LoginCancel:
ExitApp


Init:
Gui, Add, GroupBox, x12 y10 w70 h85 , Required
Gui, Add, CheckBox, x22 y30 w50 h20 vchkSPIN Checked, SPIN
Gui, Add, CheckBox, x22 y50 w50 h20 vchkSEP Checked, SEP
Gui, Add, CheckBox, x22 y70 w50 h20 vchkTMG Checked, TMG
Gui, Add, GroupBox, x92 y10 w160 h85 , Optional
Gui, Add, CheckBox, x102 y30 w50 h20 vchkSAP , SAP
Gui, Add, CheckBox, x102 y50 w80 h20 vchkLotusNotes gToggleArdexus, Lotus Notes
Gui, Add, CheckBox, x122 y70 w60 h20 vchkArdexus Disabled, Ardexus
Gui, Add, CheckBox, x185 y30 w65 h20 vchkHPSDM , HP SDM
;~ Gui, Add, CheckBox, x102 y90 w110 h20 vchkVPNWallpaper, VPN/Wallpaper
;~ Gui, Font, s9, Courier New
Gui, Font, s8, Tahoma
Gui, Add, Edit, x1 y110 w350 r15 ReadOnly vStatusLog HWNDEdit_ID,
Gui, Font
;~ Gui, Add, Progress, x1 y100 w380 h10 , 
Gui, Add, Button, x260 y15 w80 h40 Default vbtnInstall gInstall, Install
Gui, Add, Button, x260 y75 w80 h20 vbtnJoinDom gJoinDom, Domain Join
; Generated using SmartGUI Creator for SciTE
Gui, Show, w350 h320, InstallApps %ia_ver%

AddStatus("Temp: " . ia_tempdir)
AddStatus("Computer name: " . A_ComputerName)
AddStatus("Logged in as: " . ia_domain . "\" . ia_user)
AddStatus("Processor architecture: " . is64bit() . "-bit",2)
IsWow64Process := is64bit()
^F11::ListVars
return
;####################################################
;####################################################
;################## END OF AUTOEXEC #################
;####################################################
;####################################################

JoinDom:
	;~ MsgBox, 292,,Join computer to SPIN domain?
	SetTimer, ChangeJoinDomButtonNames, 50
	MsgBox, 547, InstallApps %ia_ver% - JoinDomain, Select machine type:
	IfMsgBox Yes
		JoinDomain("Notebooks")	
	else
		JoinDomain("Workstations")
return

Install:
	Gosub, DisableButtons
	FileCreateDir, %ia_tempdir%
	;~ GuiControlGet, chkSEP
	;~ GuiControlGet, chkTMG
	;~ GuiControlGet, chkLotusNotes
	;~ GuiControlGet, chkArdexus
	;~ GuiControlGet, chkSAP
	Gui, Submit, NoHide
	AddStatus("Selected to install: ",0)
	AddStatus(chkSPIN ? "SPIN " : "",0)
	AddStatus(chkSEP ? "SEP " : "",0)
	AddStatus(chkTMG ? "TMG " : "",0)
	AddStatus(chkSAP ? "SAP " : "",0)
	AddStatus(chkLotusNotes ? "LotusNotes " : "",0)
	AddStatus(chkArdexus ? "Ardexus " : "",0)
	AddStatus(chkHPSDM ? "HPSDM " : "",0)
	;~ AddStatus(chkHPSSM ? "HPSSM " : "",0)
	AddStatus()
	
	If chkSPIN
	{
		NeedReboot = 1
		RunWait, %comspec% /c net use Z: \\BSW2KP93.spin.local\prod %ia_pass% /USER:%ia_domain%\%ia_user% /PERSISTENT:NO,,Hide
		AddStatus("Setting local administrator password...",0)
		runwait, %comspec% /c net user Administrator installus,, Hide UseErrorLevel
		If ErrorLevel
		{
			GetError(ErrorLevel)
		} else {
			AddStatus("Done!")
		}
		
		;~ AddStatus("Adding SPIN US admin groups...",0)
		;~ runwait, %comspec% /c net localgroup administrators "spin\us_g_it_adm" /add,, Hide UseErrorLevel
		;~ If ErrorLevel
		;~ {
			;~ GetError(ErrorLevel)
		;~ } else {
			;~ AddStatus("Done!")
		;~ }
	
		AddStatus("Installing SPIN VPN profiles...",0)
		vpncputype := ((IsWow64Process = 64) ? " (x86)" : "")
		RunWait, %comspec% /c xcopy "Z:\public\IT\VPN Profiles\*.pcf" "C:\Program Files%vpncputype%\Cisco Systems\VPN Client\Profiles\" /Y,,Hide UseErrorLevel
		If ErrorLevel
		{
			GetError(ErrorLevel)
		} else {
			AddStatus("Done!")
		}
		
		AddStatus("Setting US VPN as default profile...",0)
		IniRead, VPNDefaultProfile, C:\Program Files%vpncputype%\Cisco Systems\VPN Client\vpnclient.ini, GUI, DefaultConnectionEntry, %A_Space%
		If VPNDefaultProfile
		{
			IniDelete, C:\Program Files%vpncputype%\Cisco Systems\VPN Client\vpnclient.ini, GUI, DefaultConnectionEntry
			VPNDefaultProfileResult = Overwrote!
		}
		else
		{
			VPNDefaultProfileResult = Done!
		}
		IniWrite, VPN to Spielo-Int US, C:\Program Files%vpncputype%\Cisco Systems\VPN Client\vpnclient.ini, GUI, DefaultConnectionEntry
		If ErrorLevel
		{
			GetError(ErrorLevel)
		} else {
			AddStatus(VPNDefaultProfileResult)
		}
		
		AddStatus("Creating VPN desktop shortcut...",0)
		RunWait, %comspec% /c xcopy "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Cisco Systems VPN Client\VPN Client.lnk" "C:\Users\Public\Desktop\" /Y,,Hide UseErrorLevel
		If ErrorLevel
		{
			GetError(ErrorLevel)
		} else {
			AddStatus("Done!")
		}
		
		AddStatus("Installing SPIN wallpaper...",0)
		RunWait, %comspec% /c xcopy "Z:\public\IT\SPIELO International_reverse_wallpaper.png" %A_Windir%\Web\Wallpaper\ /Y,,Hide UseErrorLevel
		If ErrorLevel
		{
			GetError(ErrorLevel)
		} else {
			AddStatus("Done!")
		}
		AddStatus("Setting default wallpaper...",0)
		;~ Regwrite, REG_SZ, HKU, .DEFAULT\Control Panel\Desktop, Wallpaper, %A_Windir%\Web\Wallpaper\SPIELO International_reverse_wallpaper.png
		RunWait, %comspec% /c xcopy "Z:\public\IT\wallpaper_registry\Registry.pol" %A_Windir%\system32\GroupPolicy\User\ /Y,,UseErrorLevel
		If ErrorLevel
		{
			GetError(ErrorLevel)
		} else {
			AddStatus("Done!")
		}
		;~ AddStatus("Setting default wallpaper format...",0)
		;~ Regwrite, REG_SZ, HKU, .DEFAULT\Control Panel\Desktop, WallpaperStyle, 0
		;~ If ErrorLevel
		;~ {
			;~ GetError(ErrorLevel)
		;~ } else {
			;~ AddStatus("Done!")
		;~ }
		AddStatus("Setting SPIN proxy...",0)
		RunWait, %comspec% /c xcopy "Z:\public\IT\proxy_registry" %A_Windir%\system32\GroupPolicy\User\MICROSOFT\ /Y,,UseErrorLevel
		If ErrorLevel
		{
			GetError(ErrorLevel)
		} else {
			AddStatus("Done!")
		}
		AddStatus("Installing IPHelper...",0)
		FileCreateShortcut, C:\SPIN_tools\IPHelper.exe, C:\Users\Public\Desktop\IPHelper.lnk, C:\SPIN_tools
		RunWait, %comspec% /c xcopy "Z:\public\IT\IPHelper\IPHelper.exe" C:\SPIN_tools\ /y,,Hide UseErrorLevel
		If ErrorLevel
		{
			GetError(ErrorLevel)
		} else {
			AddStatus("Done!")
		}
	}
	
	If chkSEP 
	{
		AddStatus("Installing SEP...",0)
		sepcputype := is64bit()
		RunWait, %comspec% /c net use Y: "\\Usswds01.spin.local\symantec sep" %ia_pass% /USER:%ia_domain%\%ia_user% /PERSISTENT:NO,,Hide
		RunWait, "Y:\Client %sepcputype% Bit\My Company\setup.exe",,UseErrorLevel
		If ErrorLevel
		{
			GetError(ErrorLevel)
		} else {
			;~ RunWait, %comspec% /c net use Z: /DELETE,,Hide
			AddStatus("Done!")
		}
	}
	
	If chkTMG
	{
		NeedReboot = 1
		AddStatus("Installing TMG...",0)
		RunWait, %comspec% /c net use Z: \\BSW2KP93.spin.local\prod %ia_pass% /USER:%ia_domain%\%ia_user% /PERSISTENT:NO,,Hide
		tmgcputype := ((IsWow64Process = 64) ? "amd64" : "x86")
		RunWait, msiexec /i "Z:\progs\TMGClient_ENU_%tmgcputype%\MS_FWC.msi" SERVER_NAME_OR_IP=USSTMG01 ENABLE_AUTO_DETECT=0 REFRESH_WEB_PROXY=0 /passive /norestart,,UseErrorLevel
		If ErrorLevel
		{
			GetError(ErrorLevel)
		} else {
			;~ RunWait, %comspec% /c net use Z: /DELETE,,Hide
			AddStatus("Done!")
		}
	}
	
	If chkSAP
	{
		AddStatus("Installing SAP...",0)
		;~ RunAs, %ia_user%, %ia_pass%, %ia_domain%
		RunWait, %comspec% /c net use Z: \\BSW2KP93.spin.local\prod %ia_pass% /USER:%ia_domain%\%ia_user% /PERSISTENT:NO ;,,Hide
		;~ RunWait, "Z:\public\IT\SAP\SAP_Gui_720_Atronic\Install_SAP_Gui_720_Atronic_US.exe","Z:\public\IT\SAP\SAP_Gui_720_Atronic",UseErrorLevel
		RunWait, Z:\public\it\sap\SAP_Gui_720_Atronic\SAP_2011329_1428.exe /silent,,UseErrorLevel
		If ErrorLevel
		{
			GetError(ErrorLevel)
		} else
			AddStatus("Done!")
		AddStatus("Copying saplogon.ini...",0)
		RunWait, %comspec% /c xcopy Z:\public\it\sap\saplogon.ini %A_Windir% /y,,Hide UseErrorLevel
		If ErrorLevel
		{
			GetError(ErrorLevel)
		} else {
			AddStatus("Done!")
		}
		AddStatus("Copying LocalLAT.txt...",0)
		RunWait, %comspec% /c xcopy "Z:\public\IT\SAP\ISA_Exception\LocalLAT.txt" "C:\Users\All Users\Microsoft\Firewall Client 2004\" /Y,,Hide UseErrorLevel
		If ErrorLevel
		{
			GetError(ErrorLevel)
		} else {
			AddStatus("Done!")
		}
		;~ AddStatus("Installing SAP_UPDATE...",0)
		;~ RunWait, "Z:\public\IT\SAP\SAP_UPDATE.bat","Z:\public\IT\SAP",UseErrorLevel
		;~ If ErrorLevel
		;~ {
			;~ GetError(ErrorLevel)
		;~ } else {
			;~ RunWait, %comspec% /c net use Z: /DELETE,,Hide
			;~ AddStatus("Done!")
		;~ }
	}
	
	If chkLotusNotes
	{
		AddStatus("Installing LotusNotes...",0)
		RunWait, %comspec% /c net use X: \\BSW2KP93.spin.local\mis %ia_pass% /USER:%ia_domain%\%ia_user% /PERSISTENT:NO,,Hide
		notecputype := ((IsWow64Process = 64) ? " (x86)" : "")
		RunWait, msiexec /i "X:\Applications\Installation Folder\Programs\Lotus Notes\Notes 6.5.4\Lotus Notes 6.5.4.msi" DATADIR="C:\program files%notecputype%\notes\data" PROGDIR="C:\program files%notecputype%\notes" USENOTESFOREMAIL=0 /passive /norestart,,UseErrorLevel
		If ErrorLevel
		{
			GetError(ErrorLevel)
		} else {
			AddStatus("Done!")
		}
		AddStatus("Creating Lotus Notes shortcut...",0)

		EnvGet, UserProfile, USERPROFILE
		EnvGet, AllUsers, PUBLIC
		FileDelete, %UserProfile%\Desktop\Lotus Notes 6.5.lnk
		FileCreateShortcut, C:\Program Files%notecputype%\notes\notes.exe, %AllUsers%\Desktop\Lotus Notes 6.5.lnk, C:\Program Files%notecputype%\notes\
		If ErrorLevel
		{
			GetError(ErrorLevel)
		} else {
			AddStatus("Done!")
		}
	}
	
	If chkArdexus
	{
		AddStatus("Installing Ardexus Main...",0)
		RunWait, "X:\Applications\Installation Folder\Programs\Ardexus Manual Install\Main Installation\Setup.exe" /s /v/qn,,UseErrorLevel
		If ErrorLevel
		{
			GetError(ErrorLevel)
		} else {
			AddStatus("Done!")
		}
		
		AddStatus("Installing Ardexus System...",0)
		RunWait, "X:\Applications\Installation Folder\Programs\Ardexus Manual Install\System\Setup.exe" /s /v/qn,,UseErrorLevel
		If ErrorLevel
		{
			GetError(ErrorLevel)
		} else {
			AddStatus("Done!")
		}
		
		AddStatus("Installing Ardexus SQL...",0)
		RunWait, "X:\Applications\Installation Folder\Programs\Ardexus Manual Install\SQL\Setup.exe",,UseErrorLevel
		If ErrorLevel
		{
			GetError(ErrorLevel)
		} else {
			AddStatus("Done!")
		}
		
		AddStatus("Installing Ardexus Help...",0)
		RunWait, "X:\Applications\Installation Folder\Programs\Ardexus Manual Install\Help\Setup.exe" /s /v/qn,,UseErrorLevel
		If ErrorLevel
		{
			GetError(ErrorLevel)
		} else {
			AddStatus("Done!")
		}
		
		AddStatus("Installing Ardexus Dialer...",0)
		RunWait, "X:\Applications\Installation Folder\Programs\Ardexus Manual Install\Dialer\Setup.exe" /s /v/qn,,UseErrorLevel
		If ErrorLevel
		{
			GetError(ErrorLevel)
		} else {
			AddStatus("Done!")
		}
		
		AddStatus("Installing Ardexus Mode Report...",0)
		RunWait, "X:\Applications\Installation Folder\Programs\Ardexus Manual Install\Mode Report\Setup.exe" /s /v/qn,,UseErrorLevel
		If ErrorLevel
		{
			GetError(ErrorLevel)
		} else {
			AddStatus("Done!")
		}
		
		AddStatus("Installing Ardexus Sales Report...",0)
		RunWait, "X:\Applications\Installation Folder\Programs\Ardexus Manual Install\Sales Report\Setup.exe" /s /v/qn,,UseErrorLevel
		If ErrorLevel
		{
			GetError(ErrorLevel)
		} else {
			AddStatus("Done!")
		}
		
		AddStatus("Installing Ardexus Proposal...",0)
		RunWait, "X:\Applications\Installation Folder\Programs\Ardexus Manual Install\Proposal\Setup.exe" /s /v/qn,,UseErrorLevel
		If ErrorLevel
		{
			GetError(ErrorLevel)
		} else {
			AddStatus("Done!")
		}
	}
	
	If chkHPSDM
	{
		AddStatus("Running HP SDM...",0)
		RunWait, %comspec% /c net use Z: \\BSW2KP93.spin.local\prod %ia_pass% /USER:%ia_domain%\%ia_user% /PERSISTENT:NO,,Hide
		RunWait, msiexec /i "Z:\Public\IT\HP\SDM.msi" /passive /norestart,,UseErrorLevel
		If ErrorLevel
		{
			GetError(ErrorLevel)
		} else {
			AddStatus("Done!")
		}
	}
	
	;~ If chkHPSSM
	;~ {
		;~ NeedReboot = 1
		;~ AddStatus("Running HP SSM...",0)
		;~ RunWait, \\aait\ssm\ssm.exe \\aait\ssm /accept /force /noreboot,,UseErrorLevel
		;~ If ErrorLevel
		;~ {
			;~ GetError(ErrorLevel)
		;~ } else {
			;~ AddStatus("Done!")
		;~ }
	;~ }
	
	;######################################
	;######### END OF INSTALL #############
	;######################################
	AddStatus("Finished!",3)
	Guicontrolget, txtStatusLog,, StatusLog
	LogFileName = ia_%A_ComputerName%_%A_YYYY%%A_MMM%%A_DD%_%A_Hour%%A_Min%%A_Sec%.log
	FileAppend, %txtStatusLog%, %A_Temp%\%LogFileName%
	Gosub, EnableButtons
	If NeedReboot {
		MsgBox, 308, InstallApps %ia_ver%, Reboot?
		IfMsgBox Yes
		{
			Shutdown, 6
		}
	}
return

ToggleArdexus:
	GuiControlGet, chkLotusNotes
	GuiControl,,chkArdexus, %chkLotusNotes%
	if chkLotusNotes
		GuiControl,Enable,chkArdexus
	else
		GuiControl,Disable,chkArdexus
return

AddStatus(TextToLog="", NewLines=1) {
	;~ local NewLine=
	;~ local CurrentStatus=
	;~ GuiControlGet, CurrentStatus,, StatusLog
	NewLine =
	Loop %NewLines%
		NewLine := NewLine . "`r`n"
	;~ GuiControl,, StatusLog, %CurrentStatus%%TextToLog%%NewLine%
	SendMessage 0xB1, -2, -1, , ahk_id %Edit_ID% 
	Control, EditPaste, %TextToLog%%NewLine%, , ahk_id %Edit_ID%
}

GetError(ErrorLvl) {
	global
	RunWait, %comspec% /c net helpmsg %ErrorLvl% > %ia_tempdir%\errorcode.txt,,Hide
	FileReadLine, ErrorMsg, %ia_tempdir%\errorcode.txt, 2
	AddStatus("ERROR: " . ErrorMsg . " (" . ErrorLvl . ")")
}

Is64Bit() {
	;~ global
	;~ IsWow64Process=
	;~ ThisProcess := DllCall("GetCurrentProcess")
	;~ ; If IsWow64Process() fails or can not be found,
	;~ ; assume this process is not running under wow64.
	;~ ; Otherwise, use the value returned in IsWow64Process.
	;~ if DllCall("IsWow64Process", "uint", ThisProcess, "int*", IsWow64Process)
		;~ IsWow64Process = 64
	;~ else
		;~ IsWow64Process = 32
	;~ return IsWow64Process
	VarSetCapacity(si,44)
	DllCall("GetNativeSystemInfo", "uint", &si)
	if ErrorLevel {
		MsgBox, 16, InstallApps %ia_ver%, Windows XP or later required.
		ExitApp
	}
	arc := NumGet(si,0,"ushort")
	Return % arc=0 ? "32" : arc=9 ? "64" : arc=6 ? "IA64" : "UNKNOWN"
}

JoinDomain(CPUType) {
	global
	AddStatus("Joining to SPIN domain (OU: " . CPUType . ")...",0)
	Gosub, DisableButtons
	FileAppend, 
	(
	$credential = New-Object System.Management.Automation.PsCredential("%ia_domain%\%ia_user%", (ConvertTo-SecureString "%ia_pass%" -AsPlainText -Force))
	Add-Computer -DomainName "SPIN.LOCAL" -Credential $credential -OUPath ("OU=%CPUType%,OU=US,DC=SPIN,DC=LOCAL")
	), %ia_tempdir%\joindom.ps1
	RunWait, %comspec% /c "powershell Set-ExecutionPolicy Unrestricted",,Hide
	RunWait, %comspec% /c "powershell -file %ia_tempdir%\joindom.ps1",,Hide UseErrorLevel
	If ErrorLevel
	{
		GetError(ErrorLevel)
	} else {
		ia_joindomsuccess = 1
		AddStatus("Finished!")
	}
	RunWait, %comspec% /c "powershell Set-ExecutionPolicy Restricted",,Hide
	Gosub, EnableButtons
	If ia_joindomsuccess
	{
		ia_joindomsuccess =
		MsgBox, 308, InstallApps %ia_ver%, Domain join successful!`nReboot?
		IfMsgBox Yes
		{
			Shutdown, 6
		}
	}
}

ChangeJoinDomButtonNames:
	IfWinNotExist, InstallApps %ia_ver% - JoinDomain
		return  ; Keep waiting.
	SetTimer, ChangeJoinDomButtonNames, off 
	WinActivate 
	ControlSetText, Button1, &Notebook
	ControlSetText, Button2, &Workstation
Return

DisableButtons:
	GuiControl, Disable, btnJoinDom
	GuiControl, Disable, btnInstall
return

EnableButtons:
	GuiControl, Enable, btnJoinDom
	GuiControl, Enable, btnInstall
return

AbortInstall:
	AddStatus()
	AddStatus("InstallApps Aborted!")
return

Cleanup:
	IfExist, %ia_tempdir%
		FileRemoveDir, %ia_tempdir%, 1
return

LoginGuiClose:
ExitApp

GuiClose:
	;~ FileAppend, %txtStatusLog%, \\AAIT.SPIN.LOCAL\InstallApps\Logs\%LogFileName%
	gosub Cleanup
ExitApp